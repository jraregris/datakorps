# Datakorps

> When the users sleep  
> and their screens go dark  
> the programs assemble  
> to play beautiful music

## About

### Goals

To enable myself to make and think about music in noveler ways.

### Non-goals

Being a one-size-fits all tool that everyone will enjoy.

### Inspiration

- Modular synthesis in general
- Mutable Instruments Grids <https://mutable-instruments.net/modules/grids/>
- ORCΛ <https://github.com/Hundredrabbits/Orca>
- Fasttracker2 <https://web.archive.org/web/19980530083017/http://www.starbreeze.com/ft2.htm>

## Requirements

Datakorps depends on PortMidi (<http://portmedia.sourceforge.net/portmidi/)> for MIDI I/O. On Macos, you can install with `brew install portmidi`. Note that you might have to recompile Datakorps after you install `portmidi`.

## Setup

Datakorps uses MIDI to play beautiful music. So you need to hook up some MIDI gear. For virtual MIDI action on Macos, I followed these instructions (<https://feelyoursound.com/setup-midi-os-x/).> I then set Bitwig up to listen to the MIDI events. But you can use anything you like.

The bundled drummer will play a beat on MIDI channel 10. Kick on C1, snare on C#1 and hats on D1.

## Usage

Right now, you can start the show with `iex -S mix`, and, if you have hooked everything up in a working way, it will play a little beat.
