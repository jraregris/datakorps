defmodule Datakorps.Bass do
  use GenServer

  @bar 3
  @channel 0
  @base 24
  @rounds_til_reset 4 * 4

  def start_link(opts) do
    GenServer.start_link(
      __MODULE__,
      %{
        count: 1,
        rounds: 1
      },
      opts
    )
  end

  def init(state) do
    {:ok, state}
  end

  def handle_cast(:tick, state) do
    every_tick(state[:count])
    {count, rounds} = update_count(state[:count], state[:rounds])

    {:noreply, %{count: count, rounds: rounds}}
  end

  def handle_info(_whatever, state) do
    {:noreply, state}
  end

  def every_tick(tick) do
    case tick do
      1 ->
        note(0)

      2 ->
        note(10)

      3 ->
        note(12)

      4 ->
        note(10)

      5 ->
        note(15)

      6 ->
        note(3)

      _ ->
        nil
    end
  end

  defp chance(n, true) do
    :random.uniform(n) == n
  end

  defp chance(n, f) do
    if(chance(n, true), do: f.())
  end

  defp note(n \\ 0) do
    GenServer.cast(Datakorps.Midi, {:note_on, @channel, @base + n})
    IO.puts("do #{n}")
  end

  defp update_count(count, rounds) do
    c =
      cond do
        rounds == @rounds_til_reset ->
          1

        count >= @bar ->
          cond do
            chance(4, true) -> 2
            chance(2, true) -> 4
            chance(4, true) -> 5
            chance(2, true) -> 6
            true -> 1
          end

        true ->
          count + 1
      end

    r =
      if(rounds == @rounds_til_reset) do
        1
      else
        rounds + 1
      end

    {c, r}
  end
end
