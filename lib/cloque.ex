defmodule Datakorps.Cloque do
  use GenServer

  @beat 140

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  def init(_state) do
    schedule_work()

    recievers = [Datakorps.Drummer, Datakorps.Bass]

    {:ok, recievers}
  end

  def schedule_work() do
    Process.send_after(self(), :tick, @beat)
  end

  def handle_info(:tick, recievers) do
    schedule_work()

    Enum.each(recievers, fn reciever ->
      GenServer.cast(reciever, :tick)
    end)

    {:noreply, recievers}
  end
end
