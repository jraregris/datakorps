defmodule Datakorps do
  use Application

  def start(_type, _args) do
    Datakorps.Supervisor.start_link([])
  end
end
