defmodule Datakorps.Drummer do
  use GenServer

  @bar 8

  def start_link(opts) do
    GenServer.start_link(
      __MODULE__,
      %{
        count: 1
      },
      opts
    )
  end

  def init(state) do
    {:ok, state}
  end

  def handle_cast(:tick, state) do
    every_tick(state[:count])
    count = update_count(state[:count])

    {:noreply, %{count: count}}
  end

  def handle_info(_whatever, state) do
    {:noreply, state}
  end

  def every_tick(tick) do
    case tick do
      1 ->
        bd()

      2 ->
        chance(3, &bd/0)

      3 ->
        hh()

      5 ->
        sd()

      7 ->
        chance(3, &sd/0)

      _ ->
        nil
    end
  end

  defp chance(n, f) do
    if(:random.uniform(n) == n, do: f.())
  end

  defp bd() do
    GenServer.cast(Datakorps.Midi, :bd)
    IO.puts("boom")
  end

  defp sd() do
    GenServer.cast(Datakorps.Midi, :sd)
    IO.puts("tsh")
  end

  defp hh() do
    GenServer.cast(Datakorps.Midi, :hh)
    IO.puts("chk")
  end

  defp update_count(count) do
    cond do
      count == @bar -> 1
      true -> count + 1
    end
  end
end
