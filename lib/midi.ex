defmodule Datakorps.Midi do
  use GenServer

  def start_link(opts) do
    GenServer.start_link(__MODULE__, %{}, opts)
  end

  def init(_args) do
    with [output_device | _] <- PortMidi.devices()[:output],
         {:ok, output} <- PortMidi.open(:output, output_device |> Map.get(:name)) do
      IO.puts("yay")
      IO.inspect(output)
      {:ok, output}
    else
      {:devices, _} -> exit(:no_midi)
    end
  end

  def handle_cast({:midi_msg, nil}, output) do
    msg = {0x99, 36, 127}
    IO.puts("Default msg! {0x99, 36, 127}")
    PortMidi.write(output, msg)
    {:noreply, output}
  end

  def handle_cast({:midi_msg, msg}, output) do
    IO.puts("Midi msg:")
    IO.inspect(msg)
    PortMidi.write(output, msg)

    {:noreply, output}
  end

  def handle_cast(:bd, output) do
    PortMidi.write(output, {0x99, 36, 127})

    {:noreply, output}
  end

  def handle_cast(:sd, output) do
    PortMidi.write(output, {0x99, 37, 127})

    {:noreply, output}
  end

  def handle_cast(:hh, output) do
    PortMidi.write(output, {0x99, 38, 127})
    {:noreply, output}
  end

  def handle_cast({:note_on, channel, note}, output) do
    PortMidi.write(output, {0x90 + channel, note, 127})
    Process.send_after(self(), {:note_off, channel, note}, 100)
    {:noreply, output}
  end

  def handle_info({:note_off, channel, note}, output) do
    PortMidi.write(output, {0x80 + channel, note, 127})
    {:noreply, output}
  end
end
