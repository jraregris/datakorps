defmodule Datakorps.Supervisor do
  use Supervisor

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    children = [
      {Datakorps.Cloque, name: Datakorps.Cloque},
      {Datakorps.Drummer, name: Datakorps.Drummer},
      {Datakorps.Bass, name: Datakorps.Bass},
      {Datakorps.Midi, name: Datakorps.Midi}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
