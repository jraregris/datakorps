defmodule Datakorps.MixProject do
  use Mix.Project

  def project do
    [
      app: :datakorps,
      version: "0.1.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      mod: {Datakorps, []},
      extra_applications: [:logger],
      applications: [:portmidi]
    ]
  end

  defp deps do
    [{:portmidi, "~> 5.0"}]
  end
end
